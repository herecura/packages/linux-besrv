# vim:set ft=sh:
# Maintainer: BlackEagle < ike DOT devolder AT gmail DOT com >
# Contributor: Tobias Powalowski <tpowa@archlinux.org>
# Contributor: Thomas Baechler <thomas@archlinux.org>

_kernelname=-besrv
pkgbase="linux$_kernelname"
pkgname=("linux$_kernelname" "linux$_kernelname-headers")
_basekernel=6.13
_patchver=6
if [[ "$_patchver" == rc* ]]; then
    _tag=v${_basekernel}-${_patchver}
    pkgver=${_basekernel}${_patchver}
elif [[ $_patchver -ne 0 ]]; then
    _tag=v${_basekernel}.${_patchver}
    pkgver=${_basekernel}.${_patchver}
else
    _tag=v${_basekernel}
    pkgver=${_basekernel}
fi

_folder="linux-stable"
#_gitrepo="$_folder::git+https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git?signed#tag=${_tag}"
_gitrepo="$_folder::git+https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux?signed#tag=${_tag}"
if [[ "$_patchver" == rc* ]]; then
    _folder="linux-mainline"
    _gitrepo="$_folder::git+https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git?signed#tag=${_tag}"
fi

pkgrel=1
arch=('x86_64')
license=('GPL2')
makedepends=(
    'git' 'bc' 'kmod' 'python'
    'cpio' 'tar' 'xz'  # IKERNEL_HEADERS
    'clang' 'llvm' 'lld'  # CLANG build
)
url="http://www.kernel.org"
options=(!strip)

validpgpkeys=(
    'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # "Linus Torvalds"
    '647F28654894E3BD457199BE38DBBDC86092693E'  # "Greg Kroah-Hartman"
    'E27E5D8A3403A2EF66873BBCDEA66FF797772CDC'  # "Sasha Levin"
)

source=(
    "$_gitrepo"
    # the main kernel config files
    'config-server.x86_64'
    # sysctl config
    'sysctl-linux-server.conf'
)

## extra patches
_extrapatches=(
    0001-arch-Kconfig-Default-to-maximum-amount-of-ASLR-bits.patch
)
if [[ ${#_extrapatches[@]} -ne 0 ]]; then
    source=( "${source[@]}"
        "${_extrapatches[@]}"
    )
fi

sha512sums=('dec62beb759ab8f58394a01a3a189dcfa611c7e4d9b88d86dc43cd60b94ed42f4ab06f658693ae2ffbee1373b09bfa2a16d6b06e6f2011834e36c73f752b923e'
            '510b08bf498b17f223fbc8a2d53f7f775bdb204c3d93347ee0c6ceabb9851e3efa04bf1901d8f671145238581d26d1f2519acab2cc9dae721dd0273d2a330f6a'
            '651e94c285cef48e600cf42a66651c71f3a9c7774b16c54936c35e1485a35382777ef84b3f01bf036c95fa141c941eae33867fd605c91e5f8b47617e0c0ef0c3'
            '0856e755a107ce79353a4c6c9eb80690aff05d123c48b197a64569d0a3376cd331295fd423d3f9b50e789d7f9027268c371b7aa7035884c2756c512f23b11c5a')

export KBUILD_BUILD_HOST=blackeagle
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
export LOCALVERSION=
export KERNEL_BUILD_FLAGS='LLVM=1 LLVM_IAS=1'  # CLANG
#export KERNEL_BUILD_FLAGS=''  # GCC

prepare() {
    cd "$srcdir/$_folder"

    # extra patches
    for patch in ${_extrapatches[@]}; do
        patch="$(basename "$patch" | sed -e 's/\.\(gz\|bz2\|xz\)//')"
        pext=${patch##*.}
        if [[ "$pext" == 'patch' ]] || [[ "$pext" == 'diff' ]]; then
            msg2 "apply $patch"
            patch -Np1 -i "$srcdir/$patch"
        fi
    done

    # set configuration
    msg2 "copy configuration"
    cp "$srcdir/config-server.x86_64" ./.config
    if [[ "$_kernelname" != "" ]]; then
        sed -i "s|CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\"\U$_kernelname\"|g" ./.config
    fi

    # set build salt
    sed -i "s|CONFIG_BUILD_SALT=.*|CONFIG_BUILD_SALT=\"\U$pkgbase-$pkgver-$pkgrel\"|g" ./.config

    # set extraversion to pkgrel
    msg2 "set extraversion to $pkgrel"
    if [[ "$_patchver" == rc* ]]; then
        sed -ri "s|^(EXTRAVERSION =).*|\1 ${_patchver}-$pkgrel|" Makefile
    else
        sed -ri "s|^(EXTRAVERSION =).*|\1 -$pkgrel|" Makefile
    fi
}

build() {
    cd "$srcdir/$_folder"

    if [[ -n $KERNEL_CONFIG ]]; then
        msg2 "prepare"
        make $KERNEL_BUILD_FLAGS prepare
        # Configure the kernel. Replace the line below with one of your choice.
        make $KERNEL_BUILD_FLAGS nconfig # preferred CLI menu for configuration
        ##make $KERNEL_BUILD_FLAGS menuconfig # CLI menu for configuration
        return 1
    else
        msg2 "olddefconfig"
        make $KERNEL_BUILD_FLAGS olddefconfig
    fi

    # build!
    make $KERNEL_BUILD_FLAGS -s kernelrelease > version
    msg2 "build"
    make $KERNEL_BUILD_FLAGS $MAKEFLAGS bzImage modules
}

package_linux-besrv() {
    pkgdesc="The Linux Kernel and modules, BlackEagle Server Edition"
    depends=('coreutils' 'kmod>=26' 'initramfs')
    optdepends=(
        'crda: to set the correct wireless channels of your country'
        'linux-firmware: when having some hardware needing special firmware'
    )
    provides=(
        'linux'
        'VIRTUALBOX-GUEST-MODULES'
        'WIREGUARD-MODULE'
        'KSMBD-MODULE'
    )

    cd "$srcdir/$_folder"
    local kernver="$(<version)"
    local modulesdir="$pkgdir/usr/lib/modules/$kernver"

    msg2 "Installing boot image..."
    # systemd expects to find the kernel here to allow hibernation
    # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
    install -Dm644 "$(make $KERNEL_BUILD_FLAGS -s image_name)" "$modulesdir/vmlinuz"

    # Used by initramfs to name the kernel
    echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

    msg2 "Installing modules..."
    # DEPMOD=/doesnt/exist => suppress depmod
    make $KERNEL_BUILD_FLAGS INSTALL_MOD_STRIP=1 \
        INSTALL_MOD_PATH="$pkgdir/usr" \
        DEPMOD=/doesnt/exist \
        modules_install

    # install sysctl tweaks
    install -Dm644 "$srcdir/sysctl-linux-server.conf" \
        "$pkgdir/usr/lib/sysctl.d/60-linux-besrv.conf"

    # remove build link
    rm "$modulesdir"/build
}

package_linux-besrv-headers() {
    pkgdesc="Header files and scripts for building modules for linux$_kernelname"
    provides=('linux-headers')
    depends=('clang' 'llvm' 'lld')  # make sure CLANG is installed

    KARCH=x86

    cd "$srcdir/$_folder"
    local kernver="$(<version)"
    local builddir="$pkgdir/usr/lib/modules/$kernver/build"

    msg2 "Installing build files..."
    install -Dt "$builddir" -m644 \
        .config \
        Makefile \
        Module.symvers \
        System.map \
        version
    install -Dt "$builddir/kernel" -m644 kernel/Makefile
    install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile

    # add objtool for external module building and enabled VALIDATION_STACK option
    install -Dt "$builddir/tools/objtool" tools/objtool/objtool

    if [[ -e "$builddir/tools/bpf/resolve_btfids" ]]; then
        # required when DEBUG_INFO_BTF_MODULES is enabled
        install -Dt "$builddir/tools/bpf/resolve_btfids" \
            tools/bpf/resolve_btfids/resolve_btfids
    fi

    msg2 "Installing headers..."
    find . -path './include/*' -prune -o -path './scripts/*' -prune \
        -o -type f \( -name 'Makefile*' -o -name 'Kconfig*' \
        -o -name 'Kbuild*' -o -name '*.sh' -o -name '*.pl' \
        -o -name '*.lds' \) | bsdcpio -pdm "$builddir"
    cp -t "$builddir" -a scripts include
    find $(find arch/$KARCH -name include -type d -print) -type f \
        | bsdcpio -pdm "$builddir"
    find $(find arch/$KARCH -name kernel -type d -print) -iname '*.s' -type f \
        | bsdcpio -pdm "$builddir"


    # add objtool for external module building and enabled VALIDATION_STACK option
    install -Dt "$builddir/tools/objtool" tools/objtool/objtool

    msg2 "Removing unneeded architectures..."
    local arch
    for arch in "$builddir"/arch/*/; do
        [[ $arch = */$KARCH/ ]] && continue
        echo "Removing $(basename "$arch")"
        rm -r "$arch"
    done

    msg2 "Removing documentation..."
    rm -r "$builddir/Documentation"

    msg2 "Removing broken symlinks..."
    find -L "$builddir" -type l -printf 'Removing %P\n' -delete

    msg2 "Removing loose objects..."
    find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

    msg2 "Stripping build tools..."
    local file
    while read -rd '' file; do
        case "$(file -bi "$file")" in
            application/x-sharedlib\;*)      # Libraries (.so)
                strip -v $STRIP_SHARED "$file" ;;
            application/x-archive\;*)        # Libraries (.a)
                strip -v $STRIP_STATIC "$file" ;;
            application/x-executable\;*)     # Binaries
                strip -v $STRIP_BINARIES "$file" ;;
            application/x-pie-executable\;*) # Relocatable binaries
                strip -v $STRIP_SHARED "$file" ;;
        esac
    done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

    msg2 "Adding symlink..."
    mkdir -p "$pkgdir/usr/src"
    ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}
